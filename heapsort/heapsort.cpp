#include <iostream>
#include <time.h>
#include <chrono>
using namespace std;
using namespace std::chrono;

const int N = 1000000;

int a[N]; // = { 20, 15, 22, 7, 2, 40, 80, 30, 10, 20 };

void swap (int & x, int & y)
{
    int t = x;
    x = y;
    y = t;
}

void heapify (int i, int k)
{
    while (2*i+1 <= k)
    {
        int v = 2*i+1;
        if (v+1 <= k && a[v+1] > a[v])
            v = v + 1;
        if (a[v] > a[i])
        {
            swap (a[i], a[v]);
            i = v;
        }
        else
        {
            i = k+1; // konec
        }
    }
}

void heapsort()
{
    for (int i = N-1; i >= 0; i--)
    {
        heapify (i, N-1);
    }

    for (int k = N-1; k >= 1; k--)
    {
        swap (a[0], a[k]);
        heapify (0, k-1);
    }
}

int main()
{   
    srand (time(nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000 + 10000 * (rand() % 10000);

    system_clock::time_point start = std::chrono::system_clock::now();

    heapsort();

    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        if (i < N-1)
            if (a[i+1] < a[i])
            {
                cout << "CHYBA: ";
                ok = false;
            }
        // cout << a[i] << endl;
    }

    system_clock::time_point stop = system_clock::now();

    double t = duration_cast <milliseconds> (stop - start).count();
    if (ok)
       cout << "O.K. " << t << " ms" << endl;
}

