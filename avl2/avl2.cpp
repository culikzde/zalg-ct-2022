#include <iostream>
#include <time.h>
using namespace std;

struct Item
{
    int key;
    int height = 1;
    Item* left = nullptr;
    Item* right = nullptr;
};

inline int h (Item* p)
{
    if (p == nullptr)
        return 0;
    else
        return p->height;
}

void update (Item* p)
{
    if (p != nullptr)
    {
        int L = h(p->left);
        int R = h(p->right);
        if (L > R)
            p->height = L + 1;
        else
            p->height = R + 1;
    }
}

void enter (Item*& branch, int value)
{
    if (branch == nullptr)
    {
        branch = new Item;
        branch->key = value;
        branch->left = nullptr;
        branch->right = nullptr;
    }
    else if (branch->key == value)
    {
        // duplicita
    }
    else if (value < branch->key)
    {
        enter(branch->left, value);
    }
    else
    {
        enter(branch->right, value);
    }

    int L = h(branch->left);
    int R = h(branch->right);

    Item* a = branch;

    if (L > R+1)
    {
        Item* b = a->left;
        if (h(b->left) >= h(b->right))
        {
            a->left = b->right;
            b->right = a;
            branch = b;
            update(a);
        }
        else
        {
            Item* c = b->right;
            b->right = c->left;
            a->left = c->right;
            c->left = b;
            c->right = a;
            branch = c;
            update(a);
            update(b);
        }
    }
    else if (L+1 < R)
    {
        Item* b = a->right;
        if (h(b->right) >= h(b->left))
        {
            a->right = b->left;
            b->left = a;
            branch = b;
            update(a);
        }
        else
        {
            Item* c = b->left;
            b->left = c->right;
            a->right = c->left;
            c->right = b;
            c->left = a;
            branch = c;
            update(a);
            update(b);
        }

    }

    update(branch);
}

void print(Item* p, int level = 0)
{
    if (p != nullptr)
    {
        for (int i = 1; i <= level; i++) cout << "   ";
        cout << p->key << " (" << p->height << ")" <<  endl;
        print (p->left, level+1);
        print (p->right, level+1);
    }
}


Item* root = nullptr;

int main()
{
    // enter(root, 5);
    // enter(root, 3);
    // enter(root, 4);
    // for (int i = 1; i <= 15; i++) enter(root, i);
    
    srand(time(nullptr));
    for (int i = 0; i < 1000000; i++)
        enter (root, rand() % 10000 + 10000 * (rand() % 10000));

    // print (root);
    cout << "O.K. " << h(root) << endl;
}

