#include <iostream>
#include <time.h>
#include <chrono>
using namespace std;
using namespace std::chrono;

const int N = 10000000;

int a[N]; // = { 20, 15, 22, 7, 2, 40, 80, 30, 10, 20 };

void swap(int& x, int& y)
{
    int t = x;
    x = y;
    y = t;
}

void quicksort(int r, int s)
{
    // cout << "quick " << r << " " << s << endl;
    int i = r;
    int k = s;
    int h = a[(r+s)/2];

    while (i <= k)
    {
        while (a[i] < h) i++;
        while (a[k] > h) k--;
        // cout << "step " << "a[" << i << "]=" << a[i] << " " << "a[" << k << "]=" << a[k] << endl;
        if (i <= k)
        {
            swap (a[i], a[k]);
            i ++;
            k --;
        }
    }

    if (r<k) quicksort(r, k);
    if (i<s) quicksort(i, s);
}

int main()
{
    srand(time(nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000 + 10000 * (rand() % 10000);

    system_clock::time_point start = system_clock::now();

    quicksort (0, N-1);

    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        if (i < N-1)
            if (a[i+1] < a[i])
            {
                cout << "CHYBA: ";
                ok = false;
            }
        // cout << a[i] << endl;
    }

    system_clock::time_point stop = system_clock::now();

    double t = duration_cast <milliseconds> (stop - start).count();
    if (ok)
        cout << "O.K. " << t << " ms" << endl;
}
