
#include <iostream>
using namespace std;

const int N = 8;

int a[N][N];
int cr[N][N];
int cs[N][N];

void jump (int i, int k, int v = 0, int i0 = -1, int k0 = -1) // skoc (i,k) 
{
    if (i >= 0 && i <= N-1 && k >= 0 && k <= N-1)
    {
        if (a[i][k] == -1 || a[i][k] > v)
        {
            a[i][k] = v;
            cr[i][k] = i0;
            cs[i][k] = k0;

            jump(i+1, k+2, v+1, i, k);
            jump(i-1, k+2, v+1, i, k);
            jump(i+1, k-2, v+1, i, k);
            jump(i-1, k-2, v+1, i, k);

            jump(i+2, k+1, v+1, i, k);
            jump(i-2, k+1, v+1, i, k);
            jump(i+2, k-1, v+1, i, k);
            jump(i-2, k-1, v+1, i, k);
        }
    }
}

void plot(int i1, int i2, int k1, int k2)
{
    for (int i = i1; i <= i2; i++) a[i][k1] = -2;

    for (int k = k1; k <= k2; k++) a[i1][k] = -2;

    for (int k = k1; k <= k2; k++) a[i2][k] = -2;

    for (int i = i1; i <= i2; i++) a[i][k2] = -2;

}

void cesta0 (int i, int k)
{
    while (i >= 0 && k >= 0)
    {
        cout << i << ", " << k << endl;
        int i0 = cr[i][k];
        k = cs[i][k];
        i = i0;
    }
}

void cesta (int i, int k)
{
    if (i >= 0 && k >= 0)
    {
        cesta (cr[i][k], cs[i][k]);
        cout << i << ", " << k << endl;
    }
}

int main()
{
    for (int i = 0; i < N; i++)
        for (int k = 0; k < N; k++)
            a[i][k] = -1; // nenavstivena policka

    // plot(2, 8, 2, 8);
    // plot(3, 7, 3, 7);

    int i = 0;
    int k = 0;
    jump (i, k);

    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            int s = a[i][k];
            if (s == -2) // prekazka
                cout << "X";
            else if (s == -1) // nenavstivena policka
                cout << ".";
            else
                cout << s;
            cout << " ";
        }
        cout << endl;
    }

    cesta (N-1, N-1);

    cout << "O.K" << endl;
}

