#include <iostream>
using namespace std;

class Item
{
public:
    int key = 0;
    Item * left = nullptr;
    Item * right = nullptr;
};

Item * root = nullptr;

Item* find(int value)
{
    Item* p = root;
    while (p != nullptr && p->key != value)
    {
        if (value < p->key)
            p = p->left;
        else
            p = p->right;
    }
    return p;
}

Item* search (Item * p, int value)
{
    if (p == nullptr)
    {
        return nullptr;
    }
    else if (p->key == value)
    {
        return p;
    }
    else if (value < p->key)
    {
        return search(p->left, value);
    }
    else /* if (p->key < value) */
    {
        return search (p->right, value);
    }
}

void insert (Item * & branch, int value)
{
    if (branch == nullptr)
    {
        branch = new Item;
        branch->key = value;
        branch->left = nullptr;
        branch->right = nullptr;
    }
    else if (branch->key == value)
    {
    }
    else if (value < branch->key)
    {
        insert (branch->left, value);
    }
    else /* if (branch->key < value) */
    {
        insert (branch->right, value);
    }
}

/*
void print(Item* p, int level = 0)
{
    if (p != nullptr)
    {
        print(p->left, level + 1);

        for (int i = 1; i <= level; i++)
            cout << "    ";
        cout << p->key << endl;

        print(p->right, level + 1);
    }
}
*/

/*
void print(Item* p, int pos = 40, int dist = 20)
{
    if (p != nullptr)
    {
        print(p->left, pos - dist / 2);

        for (int i = 1; i <= pos; i++)
            cout << " ";
        cout << p->key << endl;

        print(p->right, pos + dist / 2);
    }
}
*/

int depth (Item* p)
{
    int result = 0;
    if (p != nullptr)
    {
        result = 1;
        int L = depth (p->left);
        int R = depth (p->right);
        if (L > R)
            result = result + L;
        else
            result = result + R;
    }
    return result;
}



void printLevel(Item* p, int target, int level, int pos, int dist, int & cursor)
{
    if (p != nullptr)
    {
        if (level < target)
        {
            printLevel(p->left, target, level + 1, pos - dist, dist/2, cursor);
            printLevel(p->right, target, level + 1, pos + dist, dist/2, cursor);
        }
        else
        {
            while (cursor < pos)
            {
                cout << " ";
                cursor ++;
            }
            cout << p->key;
            cursor++;
            if (p->key > 9) cursor++;
        }
    }
}


void print (Item* p)
{
    int M = depth (p);
    cout << "---- strom ----" << endl;
    for (int t = 1; t <= M; t++)
    {
        int cursor = 0;
        printLevel (p, t, 1, 64, 32, cursor);
        cout << endl;
    }
    cout << "---- konec stromu ----" << endl;
}

void put (Item* p)
{
    if (p != nullptr)
    {
        cout << p->key;
        if (p->left != nullptr || p->right)
        {
            cout << " ( ";
            put(p->left);
            cout << " , ";
            put(p->right);
            cout << " ) ";
        }
    }
}

int main()
{
    insert (root, 5);
    insert (root, 3);
    insert (root, 7);
    insert (root, 8);
    insert(root, 1);
    insert(root, 4);
    insert(root, 6);
    insert(root, 9);

    Item * p = find(7);
    if (p == nullptr)
        cout << "nenasli" << endl;
    else
        cout << "nasli " << p->key << endl;

    print(root);
    // put (root);

    cout << endl << "O.K." << endl;
}
