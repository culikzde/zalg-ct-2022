// btree.cpp 

#include <iostream>
#include <fstream>
#include <time.h>
using namespace std;

const int Max = 4;

struct Item
{
    int cnt = 0; // pocet klicu
    int key [Max + 1]; // pole klicu, o jeden klic navic, cnt klicu
    Item* ref [Max + 2]; // pole ukazatelu na podstromy, cnt+1 vetvi
    Item ();
};

Item::Item()
{
    cnt = 0;
    for (int i = 0; i < Max + 1; i++) key[i] = -1;
    for (int i = 0; i < Max + 2; i++) ref[i] = nullptr;
}


void split(Item* p, int& top, Item*& n)
{
    int a = Max / 2;
    int b = Max - a;

    top = p->key[a]; // jde nahoru

    n = new Item;  // novy pravy prvek

    // klice
    // 0..a-1     zustavaji
    // a          jde nahoru
    // a+1..cnt-1 premistit

    // ukazatele
    // 0..a     zustavaji
    // a+1..cnt premistit

    n->cnt = b;
    for (int k = a + 1; k < p->cnt; k++) n->key[k - a - 1] = p->key[k];
    for (int k = a + 1; k < p->cnt + 1; k++) n->ref[k - a - 1] = p->ref[k];

    // vymazani
    p->cnt = a;
    for (int k = a; k < p->cnt; k++) p->key[k] = -1;
    for (int k = a + 1; k < p->cnt + 1; k++) p->ref[k] = nullptr;
}

void shift(Item* p, int inx)
{
    for (int k = p->cnt - 1; k >= inx; k--) p->key[k + 1] = p->key[k];
    for (int k = p->cnt; k >= inx; k--) p->ref[k + 1] = p->ref[k];
    p->cnt++;
}

void enter_next (Item * p, int val)
{
    int inx = 0;
    while (inx < p->cnt && p->key[inx] < val) inx++;

    Item* t = p->ref[inx];
    if (t == nullptr)
    {
        if (inx < p->cnt && p->key[inx] == val)
        {
            // duplicita
        }
        else
        {
            shift(p, inx);
            p->key[inx] = val;
            p->ref[inx + 1] = nullptr;
        }
    }
    else
    {
        enter_next (t, val);
        if (t->cnt == Max + 1)
        {
            int top;
            Item* n;
            split (t, top, n);

            shift(p, inx);
            p->key[inx] = top;
            p->ref[inx+1] = n;
        }
    }
}


void enter (Item * & p, int val)
{
    if (p == nullptr)
    {
        p = new Item;
        p->cnt = 1;
        p->key[0] = val;
        p->ref[0] = nullptr;
        p->ref[1] = nullptr;
    }
    else
    {
        enter_next (p, val);

        if (p->cnt == Max + 1)
        {
            int top; // jde nahoru
            Item* n;  // novy pravy prvek
            split(p, top, n);

            Item * r = new Item; // novy koren
            r->cnt = 1;
            r->key[0] = top;
            r->ref[0] = p; // puvodni koren
            r->ref[1] = n;

            p = r; // zmenit koren
        }
    }
}

void print(Item* p, int level =0)
{
    for (int k = 1; k <= level; k++)
        cout << "    ";
    for (int k = 0; k <= p->cnt - 1; k++)
    {
        cout << p->key[k] << " ";
    }
    cout << endl;

    for (int k = 0; k <= p->cnt; k++)
    {
        Item* t = p->ref[k];
        if (t != nullptr)
            print(t, level + 1);
    }
}

/* ---------------------------------------------------- */

void display (ofstream& f, Item* p, bool comma = false, int level = 0)
{
    if (p != nullptr)
    {
        bool list = (p->ref[0] == nullptr);
        for (int i = 1; i <= level; i++) f << "   ";
        f << "[";

        // klice v jednoduchych uvozovkach
        f << "'";
        for (int i = 0; i < p->cnt; i++)
        {
            f << p->key[i];
            if (i < p->cnt - 1) f << ", ";
        }
        f << "'";

        if (!list)
        {
            f << ","; // carka pred nasledujicimi hodnotam
            f << endl;
            for (int i = 0; i < p->cnt + 1; i++)
            {
                display(f, p->ref[i], i < p->cnt, level + 1);
            }
        }

        if (!list)
            for (int i = 1; i <= level; i++) f << "   ";
        f << "]";
        if (comma) f << ",";
        f << endl;
    }
}

void displayData(Item* p, string fileName)
{
    ofstream f(fileName);

    f << "<html>" << endl;
    f << "<head>" << endl;
    f << "<title>B-Tree with mxGraph</title>" << endl;
    f << "<script type=\"text/javascript\">" << endl;
    f << "    mxBasePath = 'http://jgraph.github.io/mxgraph/javascript/src';" << endl;
    f << "</script>" << endl;
    f << "<script type=\"text/javascript\" src=\"http://jgraph.github.io/mxgraph/javascript/src/js/mxClient.js\"></script>" << endl;
    f << "<script type=\"text/javascript\">" << endl;

    f << "    var data =" << endl;
    display(f, p);
    f << ";" << endl;

    f << "function display (graph, parent, above, items)" << endl;
    f << "{" << endl;
    f << "    var name;" << endl;
    f << "    if (Array.isArray (items))" << endl;
    f << "       name = items [0];" << endl;
    f << "    else" << endl;
    f << "       name = items;" << endl;
    f << endl;
    f << "    var v1 = graph.insertVertex (parent, null, name, 0, 0, 80, 30);" << endl;
    f << "    if (above != null)" << endl;
    f << "       var e1 = graph.insertEdge(parent, null, '', above, v1);" << endl;

    f << "    if (Array.isArray (items))" << endl;
    f << "    {" << endl;
    f << "      var i;" << endl;
    f << "      for (i = 1; i < items.length; i++)" << endl;
    f << "      {" << endl;
    f << "         var item = items [i];" << endl;
    f << "         display (graph, parent, v1, items [i]);" << endl;
    f << "      }" << endl;
    f << "    }" << endl;
    f << "    return v1;" << endl;
    f << "}" << endl;
    f << "function main (container)" << endl;
    f << "{" << endl;
    f << "    var graph = new mxGraph(container);" << endl;
    f << "    var parent = graph.getDefaultParent();" << endl;
    f << "    graph.getModel().beginUpdate();" << endl;
    f << "    var v1 = display (graph, parent, null, data);" << endl;
    f << "    var layout = new mxHierarchicalLayout (graph, mxConstants.DIRECTION_NORTH);" << endl;
    f << "    layout.execute (graph.getDefaultParent(), v1);" << endl;
    f << "    graph.getModel().endUpdate();" << endl;
    f << "};" << endl;
    f << "</script>" << endl;
    f << "</head>" << endl;
    f << "<body onload=\"main(document.getElementById('graphContainer'))\">" << endl;
    f << "<div id=\"graphContainer\">" << endl;
    f << "</div>" << endl;
    f << "</body>" << endl;
    f << "</html>" << endl;

    f.close();
}

/* ---------------------------------------------------- */

Item* root = nullptr;

int main()
{
    enter(root, 30);
    enter(root, 40);
    enter(root, 10);
    enter(root, 20);
    enter(root, 50);
    for (int k = 21; k <= 29; k++) enter(root, k);
    for (int k = 51; k <= 80; k++) enter(root, k);
    // for (int k = 1; k <= 999; k++) enter(root, k);
    // for (int k = 1; k <= 20000; k++) enter(root, k);
    print(root);

    displayData (root, "output.html");

    cout << "O.K." << endl;
}
