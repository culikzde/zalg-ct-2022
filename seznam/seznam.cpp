#include <iostream>
using namespace std;

/*
struct Item
{
    string name;
    int r;
    int g;
    int b;
};
*/

class Item
{
public:
    string name;
    int r;
    int g;
    int b;
    Item * next;

    Item (string name0 = "", int r0 = 255, int g0 = 255, int b0 = 255);
};

Item::Item (string name0, int r0, int g0, int b0)
{
    name = name0;
    r = r0;
    g = g0;
    b = b0;
    next = nullptr;
}

class List
{
   public:
       Item* first = nullptr;
       Item* last = nullptr;

       void addFirst (Item* fresh);
       void addLast (Item* fresh);
       void print ();
};

void List::addFirst (Item* fresh)
{
    fresh->next = first;
    first = fresh;
    if (last == nullptr)
    {
        last = fresh;
    }
}

void List::addLast (Item* fresh)
{
    if (last == nullptr)
    {
        fresh->next = nullptr;
        first = fresh;
        last = fresh;
    }
    else
    {
        last->next = fresh;
        fresh->next = nullptr;
        last = fresh;
    }
}

void List::print()
{
    Item* p = first;
    cout << "--- Zacatek seznamu ---" << endl;
    while (p != nullptr)
    {
        cout << p->name << " " << p->r << "," << p->g << "," << p->b << endl;
        p = p->next;
    }
    cout << "--- Konec seznamu ---" << endl;
}

List list;

int main()
{
    list.addLast (new Item ("Cervena", 255, 0, 0));
    list.addLast (new Item ("Zelena", 0, 255, 0));
    list.addLast (new Item ("Modra", 0, 0, 255));
    list.addFirst (new Item("Zluta", 255, 255, 0));

    list.print();

    cout << "O.K." << endl;
}
