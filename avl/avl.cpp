﻿#include <iostream>
using namespace std;

struct Item
{
    int key = 0;     // ulozena hodnota
    int height = 1;  // vyska stromu zacinajiciho timto prvkem
    Item* left = nullptr;  // levy podstrom s hodnotami < key (nebo nullptr, pokud levy podstrom neexistuje
    Item* right = nullptr; // pravy podstrom s honotami > key
};

inline int h (Item* p)
{
    if (p == nullptr)
        return 0;
    else
        return p->height;
}

void update (Item* p)
{
    int L = h(p->left);
    int R = h(p->right);
    if (L > R)
        p->height = L + 1; // vyska leveho podstromu + 1 za nas prvek
    else
        p->height = R + 1;
}

void enter (Item * & root, int val)
{
    if (root == nullptr)
    {
        root = new Item;
        root->key = val;
    }
    else if (val < root->key) enter (root->left, val);
    else if (val > root->key) enter (root->right, val);
    else { } // hodnota je již ve stromu uložena

    /* v tomto místě se ocitneme po vložení nového prvku do stromu
       a později když se budeme "vynořovat" z předešlých funkcí enter */
}

/* ---------------------------------------------------------------------- */

void print (Item* branch, int level = 0)
{
    if (branch != nullptr)
    {
        for (int i = 0; i < level; i++)
            cout << "-";

        cout << branch->key << " (" << branch->height << ")" << endl;

        print(branch->left, level+1);
        print(branch->right, level+1);
    }
}

bool ok;
bool first;
int  last_value;

void checkBranch(Item* branch)
{
    if (branch != nullptr)
    {
        checkBranch(branch->left);

        if (!first)
            if (last_value >= branch->key)
            {
                cout << "CHYBA" << endl;
                ok = false;
            }

        last_value = branch->key;
        first = false;
        checkBranch(branch->right);
    }
}

void check(Item* root)
{
    ok = true;
    first = true;
    checkBranch(root);
    if (ok) cout << "O.K." << endl;
}

/* ---------------------------------------------------------------------- */

int main()
{
    Item* root = nullptr;

    const int N = 8;
    // const int N = 16*1024;

    for (int k = 1; k <= N; k++)
        enter (root, k);

    if (N <= 1000)
        print(root);

    cout << "Vyska stromu " << h(root) << endl;
    check(root);
}

