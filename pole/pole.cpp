#include <iostream>
#include <cassert>
using namespace std;

typedef double T;

class Matrix
{
private:
    int line_cnt;
    int col_cnt;
    T * data = nullptr;
public:
    Matrix(int lines0, int columns0) 
       { line_cnt = lines0; col_cnt = columns0; data = new T [line_cnt*col_cnt]; }

    Matrix (Matrix & b);

    ~Matrix() { /* if (data != nullptr) delete [] data; */ }

    T & operator ( ) (int i, int k) 
    {
        // assert (i >= 1 && i <= line_cnt);
        // assert (k >= 1 && k <= col_cnt);
        // return data [(i-1) * col_cnt + (k-1)];
        assert (i >= 0 && i < line_cnt);
        assert (k >= 0 && k < col_cnt);
        return data[i * col_cnt + k];
    }

    int lines () { return line_cnt; }
    int columns () { return col_cnt; }
    void print();

};

Matrix::Matrix (Matrix& b)
{
    // if (data != nullptr) delete[] data;
    line_cnt = b.lines(); 
    col_cnt = b.columns(); 
    data = new T[line_cnt * col_cnt];
    for (int i = 0; i < line_cnt; i++)
        for (int k = 0; k < col_cnt; k++)
            (*this)(i, k) = b(i, k);
}

Matrix operator + (Matrix & a, Matrix & b)
{
    assert (a.lines() == b.lines());
    assert (a.columns() == b.columns());

    Matrix result (a.lines(), a.columns());

    for (int i = 0; i < a.lines (); i++)
        for (int k = 0; k < a.columns (); k++)
            result(i, k) = a (i, k) + b(i, k);

    return result;
}

Matrix operator * (Matrix & a, Matrix & b)
{
    assert (a.columns() == b.lines());

    Matrix result(a.lines (), b.columns());

    for (int i = 0; i < a.lines (); i++)
        for (int k = 0; k < b.columns (); k++)
        {
            T sum = 0;
            for (int n = 0; n < a.columns (); n++)
                sum = sum + a (i, n) * b(n, k);
            result(i, k) = sum;
        }

    return result;
}


void Matrix::print()
{
    cout << "[" << endl;;
    for (int i = 0; i < line_cnt; i++)
    {
        cout << "(";
        for (int k = 0; k < col_cnt; k++)
        {
            cout << (*this) (i,k);
            if (k < col_cnt - 1) cout << ", ";
        }
        cout << ")" << endl;
    }
    cout << "]" << endl;
}

int main()
{
    Matrix a (10, 10);
    Matrix b (10, 10);

    for (int i = 0; i < a.lines(); i++)
        for (int k = 0; k < a.columns(); k++)
            a(i, k) = 10 * i + k;

    for (int i = 0; i < b.lines(); i++)
        for (int k = 0; k < b.columns(); k++)
            if (i == k)
               b(i, k) = 1;
            else
                b(i, k) = 0;

    Matrix c(10, 10);
    c = a * b;

    a.print();

    cout << "O.K." << endl;
}

