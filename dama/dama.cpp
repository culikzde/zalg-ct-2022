#include <iostream>
using namespace std;

const int N = 8;

int a [N];

int cnt = 0; // pocet reseni

void tisk()
{
    cnt++;

    for (int k = 0; k < N; k++) // cisla sloupcu
    {
        cout << a[k] << " ";
    }
    cout << endl;

    for (int i = 0; i < N; i++) // cisla radek
    {
        for (int k = 0; k < N; k++) // cisla sloupcu
        {
            if (a[k] == i)
                cout << "* ";
            else
                cout << ". ";
        }
        cout << endl;
    }
    cout << endl;
}

void play (int k) // umistovat damu do k-teho sloupce, sloupce od 0 do N-1
{
    for (int i = 0; i < N; i++) // cisla radek
    {
        bool ok = true;

        for (int v = 1; v <= k; v++)
            if (a[k-v] == i || a[k-v] == i-v || a[k-v] == i+v) // dama v s-tem sloupci na radce i
               ok = false;

        if (ok)
        {
            a[k] = i; // v k-tem sloupci je dama na i-tem radku

            if (k < N-1)
                play(k+1); // umistovat na k+1 sloupec
            else
                tisk(); // mam reseni
        }
    }
}

int main()
{
    play (0);
    cout << "cnt = " << cnt << " O.K." << endl;
}

